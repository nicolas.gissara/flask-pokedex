// Ultimo pipeline configurado con etapas de testing para OWASP ZAP 21/12/2023

pipeline {
    agent any

    environment {
        TEMPLATE_PATH = "@/usr/local/share/trivy/templates/html.tpl"
        DOCKERHUB_CREDENCIALS = credentials('credentials-dockerhub')
        BUILD_NUMBER = "${BUILD_NUMBER}"
        RepoDockerHub = 'ngissara'
        NameContainer = 'devops'
        ARGOSERVER = '192.168.205.32:8090'
		REPO_URL = 'https://gitlab.com/nicolas.gissara/flask-pokedex.git'
		APP_PORT = '5000'
        ZAP_CONTAINER_NAME = 'owasp-zap'
        ARGONAMESPACE = 'argocd'
        //ARGOCD_USERNAME = credentials('argo-username-credentials') // Credenciales para el nombre de usuario
        //ARGOCD_PASSWORD = credentials('argo-password-credentials') // Credenciales para la contraseña
        //ARGOCD_TOKEN = credentials('argo-token-credentials2') // Asegúrate de crear credenciales secretas en Jenkins para el token
    }
	
    stages {
        				
        stage('Build and deployd Test OWASP ZAP') {
            steps {
                script {
                    def appContainerName = 'PokeAcu'
                    /*
                    echo "Eliminando el directorio existente..."
                    sh "rm -rf flask-pokedex"
                    
                    echo "Clonando el repositorio desde GitLab..."
                    sh "git clone ${REPO_URL}"
                    */
                    echo "Construyendo la imagen Docker..."
                    sh "docker build -t ${env.RepoDockerHub}/${env.NameContainer}:${env.BUILD_NUMBER} ."

                    // Detener y eliminar el contenedor de la aplicación si está en ejecución
                    sh "docker stop ${appContainerName} || true"
                    sh "docker rm ${appContainerName} || true"

                    // Iniciar el contenedor de la aplicación
                    sh "docker run -d --name ${appContainerName} -p ${APP_PORT}:5000 ${env.RepoDockerHub}/${env.NameContainer}:${env.BUILD_NUMBER}"
                }
            }
        }
       
        stage('Run OWASP ZAP Baseline') {
            steps {
                script {
                    //def zapContainerName = 'owasp-zap' // Nombre correcto del contenedor OWASP ZAP
                                        
                    // Verificar si el contenedor OWASP ZAP está en ejecución
                    def isZapContainerRunning = sh(script: "docker ps --format '{{.Names}}' | grep '^owasp-zap\$'", returnStatus: true) == 0

                    // Iniciar el contenedor OWASP ZAP si no está en ejecución
                    if (!isZapContainerRunning) {
                        sh 'docker run --name owasp-zap -u zap -v $(pwd)/zap-wrk:/zap/wrk -p 9090:8080 -d owasp/zap2docker-stable sleep infinity'
                    }
                    // Verificar si el directorio /zap/wrk existe
                    def directoryExists = sh(script: 'docker exec owasp-zap test -d /zap/wrk', returnStatus: true)

                    // Si el directorio no existe, créalo
                    if (directoryExists != 0) {
                        echo 'Creating /zap/wrk directory'
                        sh 'docker exec owasp-zap mkdir /zap/wrk'
                    }
                  
                    // Ejecutar el comando directamente dentro de sh
                    //sh 'docker exec owasp-zap zap-baseline.py -t http://192.168.0.59:5005 -J -r /zap/zap_report.html -x /zap/report.xml -I'
                    sh 'docker exec owasp-zap zap-baseline.py -t http://192.168.205.57:5005 -g gen.conf -r /zap/wrk/zap_report.html || true'
                    
                    // Copiar el informe fuera del contenedor Docker
                    sh 'docker cp owasp-zap:/zap/wrk/zap_report.html .'
                    
                }
            }
        }  
      
        stage('Remove Test Build ') {
            steps {
                script {
                    def imageToDelete = "${env.RepoDockerHub}/${env.NameContainer}:${env.BUILD_NUMBER}"
                    def appContainerName = 'PokeAcu'
                    echo "Deteniendo y eliminando el contenedor OWASP ZAP..."
                    sh "docker stop ${env.ZAP_CONTAINER_NAME}"
                    sh "docker rm ${env.ZAP_CONTAINER_NAME}"
                    //sh "docker rmi ${imageToDelete}"
                    
                    // Detener y eliminar el contenedor de la aplicación si está en ejecución
                    sh "docker stop ${appContainerName} || true"
                    sh "docker rm ${appContainerName} || true"
                }
            }
        }
        		
		stage('Login to Dockerhub') {
            steps {
                sh "echo $DOCKERHUB_CREDENCIALS_PSW | docker login -u $DOCKERHUB_CREDENCIALS_USR --password-stdin "
            }
        }
        stage('OWASP Dependency Scan') {
            steps {
                dependencyCheck additionalArguments: '''
                    -o "./"
                    -s "./"
                    -f "ALL"
                    --prettyPrint ''', odcInstallation: 'DP-Check'
                dependencyCheckPublisher pattern: '**/dependency-check-report.xml'
            }
        }
				
		stage('Push image to Dockerhub') {
            steps {
                sh "docker build -t ${env.RepoDockerHub}/${env.NameContainer}:latest ."
                sh "docker push ${env.RepoDockerHub}/${env.NameContainer}:${env.BUILD_NUMBER}"
                sh "docker push ${env.RepoDockerHub}/${env.NameContainer}:latest"
                // una vez subido borro el latest
                sh "docker rmi ${env.RepoDockerHub}/${env.NameContainer}:latest"
            }
        }

        stage('Analyze with Trivy') {
            steps {
                echo "Analyze with Trivy"
                sh "trivy image --format template --template ${TEMPLATE_PATH} --severity HIGH,CRITICAL -o cve_report.html ${env.RepoDockerHub}/${env.NameContainer}:${env.BUILD_NUMBER}"
            }
        }

        stage('Deploy Full to ArgoCD') {
            steps {
                script {
                    // Obtener el token de ArgoCD
                    // def argocdToken = credentials('argo-token-credentials2')

                    // Configurar los detalles de la aplicación
                    def creds = credentials('argo-credentials')
                    def appName = 'pokeaccusys'
                    def repoURL = 'https://gitlab.com/nicolas.gissara/flask-pokedex.git'
                    def targetRevision = 'main'
                    def appPath = 'manifests'
                    def namespace = 'default'
                                                
                    // Reemplazar la imagen en el archivo YAML con la última versión
                    // sh "sed -i \"s|image: ${env.RepoDockerHub}/${env.NameContainer}:.*|image: ${env.RepoDockerHub}/${env.NameContainer}:${env.BUILD_NUMBER}|g\" main/flask-pokedex/manifests/Pokemon-deploy.yml"
                    sh "sed -i 's/image: ngissara\\/devops:[0-9]*/image: ngissara\\/devops:${env.BUILD_NUMBER}/g' $WORKSPACE/manifests/Pokemon-deploy.yml"
                    
                    // Crear la aplicación en ArgoCD argocd login ${ARGOSERVER} --insecure --grpc-web --username ${username} --password ${password} &&
                    
                    sh """
                        argocd login ${ARGOSERVER} --insecure --grpc-web --username admin --password Uif12345 &&
                        argocd app create ${appName} \\
                            --repo ${repoURL} \\
                            --revision ${targetRevision} \\
                            --path ${appPath} \\
                            --dest-server https://kubernetes.default.svc \\
                            --dest-namespace ${namespace} \\
                            --sync-policy automated &&                     
                        argocd logout ${ARGOSERVER}
                     """
                }
            }
        }
    }

    post {
        always {
            script {
                archiveArtifacts artifacts: "cve_report.html", fingerprint: true
                publishHTML (target: [
                    allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: '.',
                    reportFiles: 'cve_report.html',
                    reportName: "CVE Report"
                ])
            }
        }

        success {
            script {
                // Publicar el informe HTML para visualización y descarga
                archiveArtifacts artifacts: "zap_report.html", fingerprint: true
                publishHTML (target: [
                    allowMissing: false,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: '.',
                    reportFiles: 'zap_report.html',
                    reportName: "OWASP ZAP Report"
                ])
            }
        }
    }
}